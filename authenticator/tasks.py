from celery import Celery
from authenticator.services import OTPSender
from kavenegar import APIException
app = Celery('tasks', broker='redis://localhost:6379/')


@app.task(bind=True, queue='otp')
def send_sms_task(self, phone_number, otp):
    print('================ start sendig sms ===============')
    otpsender = OTPSender(phone_number, otp)

    try:
        otpsender.send_sms()
        
    except Exception as exc:
        print(exc, '========================')
        raise self.retry(exc=exc)

    print(f'================ OTP code sent to "{phone_number}":) =================')

    