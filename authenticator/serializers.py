from rest_framework import serializers
from authenticator.models import CustomUser

class OTPLoginSerializer(serializers.Serializer):
    phone_number = serializers.CharField(max_length=11, allow_null=False)


class VerifyOTPSerializer(serializers.Serializer):
    phone_number = serializers.CharField(max_length=11, allow_null=False)
    password = serializers.CharField(max_length=64, allow_null=False)

class ObtainTokenSerializer(serializers.Serializer):
    access = serializers.CharField(max_length=128, allow_null=False)
    refresh = serializers.CharField(max_length=128, allow_null=False)