from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from authenticator.serializers import OTPLoginSerializer, VerifyOTPSerializer, ObtainTokenSerializer
from authenticator.models import CustomUser
from authenticator.tasks import send_sms_task
from django.utils import timezone
from django.contrib.auth import get_user_model
from rest_framework_simplejwt.tokens import RefreshToken



class OTPLoginView(APIView):

    def get(self, request):
        serialzer = OTPLoginSerializer(data=request.query_params)
        if serialzer.is_valid():
            data = serialzer.validated_data

            try:
                cuser = CustomUser.objects.get(username=data['phone_number'])
                cuser.save()
            except CustomUser.DoesNotExist:
                cuser = CustomUser.objects.create(username=data['phone_number'])
            
            send_sms_task.delay(phone_number=cuser.username, otp=cuser.password)

            return Response(status=status.HTTP_200_OK)
        return Response(data=serialzer.errors, status=status.HTTP_400_BAD_REQUEST)
            

    def post(self, request):
        serializer = VerifyOTPSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data

            try:
                user = CustomUser.objects.get(username=data['phone_number'], 
                password=data['password'], 
                otp_expiry_date__gte=timezone.now())
                refresh = RefreshToken.for_user(user)

                return Response(ObtainTokenSerializer({
                    'access': str(refresh.access_token),    
                    'refresh': str(refresh)

                }).data)

            except CustomUser.DoesNotExist:
                return Response(status=status.HTTP_401_UNAUTHORIZED)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
