from authenticator.views import OTPLoginView

from django.urls import path

urlpatterns = [
    path('', OTPLoginView.as_view()),
]