from kavenegar import *
from dezhpad.settings import KAVEHNEGAR_API_KEY


class OTPSender:

    def __init__(self, receptor, message):
        self.api = KavenegarAPI(KAVEHNEGAR_API_KEY)
        self.receptor = receptor
        self.message = message

    def send_sms(self):
        params = {
            'receptor' : self.receptor,
            'message' : f'your verification code is:\n {self.message}.\n Dezhpad'
        }

        response = self.api.sms_send(params)