from django.db import models
from django.contrib.auth.models import AbstractUser
import random
from django.utils import timezone
from datetime import timedelta


class CustomUser(AbstractUser):
    otp_expiry_date = models.DateTimeField()
    
    @staticmethod
    def generate_otp():
        return random.randint(12345, 100000)
    
    def save(self, *args, **kwargs):
        self.password = self.generate_otp()
        self.otp_expiry_date = timezone.now() + timedelta(seconds=120)
        super(CustomUser, self).save(*args, **kwargs)

    def __str__(self):
        return (f'user phone number: {self.username}')